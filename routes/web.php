<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\SocialiteController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\TopikController;
use App\Http\Controllers\MateriController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('layout.master');
// });

// Route::get('/', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    //Route Home Page
    Route::get('/', function () {
        return view('home');
    })->name('home');

    //Route Profile Page
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    //Route Course,Topik,Materi
    Route::get('/course', [KategoriController::class, 'read']);
    Route::get('/course/{id_kategori}', [KategoriController::class, 'showTopik']);
    Route::get('/course/{id_kategori}/{id_topik}', [TopikController::class, 'showMateri']);

    //Route About Page
    Route::get('/about', function () {
        return view('page.about');
    });
});

Route::middleware('auth', 'admin')->group(function () {
    //Route Dashboard Admin
    Route::get('/dashboard', function () {
        return view('dashboardAdmin');
    });

    //Route Read User
    Route::get('/user', [UserController::class, 'tampiluser']);

    //CRUD Kategori
    Route::resource('/kategori', KategoriController::class);

    //CRUD Topik
    Route::resource('/topik', TopikController::class);

    //CRUD Materi
    Route::resource('/materi', MateriController::class);
});

require __DIR__ . '/auth.php';

Route::get('/auth/{provider}', [SocialiteController::class, 'redirectToProvider']);
Route::get('/auth/{provider}/callback', [SocialiteController::class, 'handleProvideCallback']);