@extends('layout.master')

@section('page')
<section class="slider-area ">
    <div class="slider-active">
        <!-- Single Slider -->
        <div class="single-slider slider-height d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-7 col-md-12">
                        <div class="hero__caption">
                            <h1 data-animation="fadeInLeft" data-delay="0.2s">Platform<br> Belajar Programming</h1>
                            <p data-animation="fadeInLeft" data-delay="0.4s">Belajar coding online untuk pemula hingga
                                tingkat lanjutan. Gabung komunitas dan mulailah eksplorasi coding!</p>
                            <a href="/course" class="btn hero-btn" data-animation="fadeInLeft" data-delay="0.7s">Jelajah
                                Bahasa</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ? services-area -->
<div class="services-area">
    <div class="container">
        <div class="row justify-content-sm-center">
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="single-services mb-30">
                    <div class="features-icon">
                        <img src="{{asset('/courses-master/assets/img/icon/icon1.svg')}}" alt="">
                    </div>
                    <div class="features-caption">
                        <h3>Banyak Pilihan Bahasa Pemrograman</h3>
                        <p>Large Selection of Programming Languages</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="single-services mb-30">
                    <div class="features-icon">
                        <img src="{{asset('/courses-master/assets/img/icon/icon2.svg')}}" alt="">
                    </div>
                    <div class="features-caption">
                        <h3>Video Pembelajaran Yang Berkualitas</h3>
                        <p>Quality Learning Videos</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8">
                <div class="single-services mb-30">
                    <div class="features-icon">
                        <img src="{{asset('courses-master/assets/img/icon/icon3.svg')}}" alt="">
                    </div>
                    <div class="features-caption">
                        <h3>Akses Materi Yang Tak Terbatas</h3>
                        <p>Unlimited Access to Materials</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--? About Area-1 Start -->
<section class="about-area1 fix pt-10">
    <div class="support-wrapper align-items-center">
        <div class="left-content1">
            <div class="about-icon">
                <img src="{{asset('courses-master/assets/img/icon/about.svg')}}" alt="">
            </div>
            <!-- section tittle -->
            <div class="section-tittle section-tittle2 mb-55">
                <div class="front-text">
                    <h2 class="">Asah Potensi Pemrograman Anda dengan Mudah!</h2>
                    <p>Pelajari keterampilan baru secara online dengan video pembelajaran terbaik</p>
                </div>
            </div>
            <div class="single-features">
                <div class="features-icon">
                    <img src="{{asset('courses-master/assets/img/icon/right-icon.svg')}}" alt="">
                </div>
                <div class="features-caption">
                    <p>Materi pembelajaran yang terstruktur</p>
                </div>
            </div>
            <div class="single-features">
                <div class="features-icon">
                    <img src="{{asset('courses-master/assets/img/icon/right-icon.svg')}}" alt="">
                </div>
                <div class="features-caption">
                    <p>Waktu dan tempat yang sangat fleksibel</p>
                </div>
            </div>
            <div class="single-features">
                <div class="features-icon">
                    <img src="{{asset('courses-master/assets/img/icon/right-icon.svg')}}" alt="">
                </div>
                <div class="features-caption">
                    <p>Video pembelajaran yang berkualitas</p>
                </div>
            </div>
        </div>
        <div class="right-content1">
            <!-- img -->
            <div class="right-img">
                <img src="{{asset('courses-master/assets/img/gallery/about.png')}}" alt="">

                <div class="video-icon embed-responsive embed-responsive-16by9">
                    {{-- <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=2lVDktWK-pc"><i
                            class="fas fa-play"></i></a> --}}
                    <iframe class="popup-video btn-icon fas fa-play" width="560" height="315"
                        src="https://www.youtube-nocookie.com/embed/2lVDktWK-pc" title="YouTube video player"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Area End -->
<!--? top subjects Area Start -->
<div class="topic-area section-padding40">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-8">
                <div class="section-tittle text-center mb-55">
                    <h2>Terpopuler</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/11166a84-9aa9-4afc-9e30-b25d00dfc575.webp"
                            alt="">
                        <h2><b>Laravel</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/b68dcd3f-50d4-4b09-b26a-17f22e572d5a.webp"
                            alt="">
                        <h2><b>CodeIgniter</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/d629226b-24e4-41eb-bafe-1ee86f3dc102.webp"
                            alt="">
                        <h2><b>JavaScript</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/e0a09387-ee95-42de-9048-cd352790e6fd.webp"
                            alt="">
                        <h2><b>PHP</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/f33b3b22-847a-44eb-b334-9695069dbbf9.webp"
                            alt="">
                        <h2><b>React Js</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/df6e5b68-ccbd-4c14-9eec-89789e546da3.webp"
                            alt="">
                        <h2><b>Tailwind CSS</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/fdb1a4fb-12c9-47a4-ae82-b34fdaefe888.webp"
                            alt="">
                        <h2><b>Python</b></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-topic text-center mb-30">
                    <div class="topic-img">
                        <img src="https://santrikoding.com/storage/categories/2231049a-3acc-4149-b459-848b39887a39.webp"
                            alt="">
                        <h2><b>Ruby</b></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div class="section-tittle text-center mt-20">
                    <a href="/course" class="border-btn">Lihat Lainnya</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- top subjects End -->
<!--? About Area-3 Start -->
<section class="about-area3 fix">
    <div class="support-wrapper align-items-center">
        <div class="right-content3">
            <!-- img -->
            <div class="right-img">
                <img src="{{asset('courses-master/assets/img/gallery/about3.png')}}" alt="">
            </div>
        </div>
        <div class="left-content3">
            <!-- section tittle -->
            <div class="section-tittle section-tittle2 mb-20">
                <div class="front-text">
                    <h2 class="">Belajar dengan materi-materi terbaik dan up to date</h2>
                </div>
            </div>
            <div class="single-features">
                <p>BangKode berkomitmen untuk terus mengembangkan konten pembelajaran terbaru dan terbaik</p>
            </div>
            <div class="single-features">
                <p>Pembelajaran dirancang secara khusus untuk memenuhi kebutuhan berbagai tingkat keahlian</p>
            </div>
            <div class="single-features">
                <p>Setiap materi dilengkapi dengan video tutorial berkualitas tinggi</p>
            </div>
        </div>
    </div>
</section>
<!-- About Area End -->
@endsection