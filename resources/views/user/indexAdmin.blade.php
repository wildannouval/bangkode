@extends('dashboardAdmin')

@section('title')
    List User
@endsection

@section('content')
    {{-- <a href="/topik/create" type="button" class="btn btn-success">Tambah</a> --}}
    <table class="table table-striped data">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama User</th>
                <th scope="col">Email</th>
                <th scope="col">Password</th>
                {{-- <th scope="col">Aksi</th> --}}
            </tr>
        </thead>
        <tbody>
            @forelse ($user as $key => $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->password }}</td>
                    </td>
                    {{-- <td>
            <form action="/topik/{{$item->id_topik}}" method="POST">
              @csrf
              @method('delete')
              <a href="/topik/{{$item->id_topik}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
          </form>
          </td> --}}
                </tr>
            @empty
                <div class="alert alert-danger">
                    Data User belum Tersedia.
                </div>
            @endforelse
        </tbody>
    </table>
@endsection
