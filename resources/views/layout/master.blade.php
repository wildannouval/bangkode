<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>BangKode | Belajar Coding</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/slicknav.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/progressbar_barfiller.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/gijgo.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/animated-headline.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('/courses-master/assets/css/style.css') }}">

</head>

<body>
    <!-- ? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{ asset('logo_bangkode.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        @include('partials.nav')
    </header>
    <main>
        @yield('page')
    </main>
    <footer>
        @include('partials.footer')
    </footer>
    <!-- Scroll Up -->
    <div id="back-top">
        <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
    </div>

    <!-- JS here -->
    <script src="{{ asset('/courses-master/assets/js/vendor/modernizr-3.5.0.min.js') }}"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{ asset('/courses-master/assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/bootstrap.min.js') }}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{ asset('/courses-master/assets/js/jquery.slicknav.min.js') }}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{ asset('/courses-master/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/slick.min.js') }}"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="{{ asset('/courses-master/assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/animated.headline.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/jquery.magnific-popup.js') }}"></script>

    <!-- Date Picker -->
    <script src="{{ asset('/courses-master/assets/js/gijgo.min.js') }}"></script>
    <!-- Nice-select, sticky -->
    <script src="{{ asset('/courses-master/assets/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/jquery.sticky.js') }}"></script>
    <!-- Progress -->
    <script src="{{ asset('/courses-master/assets/js/jquery.barfiller.js') }}"></script>

    <!-- counter , waypoint,Hover Direction -->
    <script src="{{ asset('/courses-master/assets/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/hover-direction-snake.min.js') }}"></script>

    <!-- contact js -->
    <script src="{{ asset('/courses-master/assets/js/contact.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/jquery.form.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/mail-script.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/jquery.ajaxchimp.min.js') }}"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="{{ asset('/courses-master/assets/js/plugins.js') }}"></script>
    <script src="{{ asset('/courses-master/assets/js/main.js') }}"></script>

</body>

</html>
