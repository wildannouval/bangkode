@extends('layout.master')

@section('page')
        <section class="slider-area slider-area2">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height2">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 col-lg-11 col-md-12">
                                <div class="hero__caption hero__caption2">
                                    <h1 data-animation="bounceIn" data-delay="0.2s">About Us</h1>
                                </div>
                            </div>
                        </div>
                    </div>          
                </div>
            </div>
        </section>
        <!--? About Area-3 Start -->
        <section class="about-area3 fix">
            <div class="support-wrapper align-items-center">
                <div class="right-content3">
                    <!-- img -->
                    <div class="right-img mt-30 ml-100">
                        <img src="{{asset('logo_bangkode.jpg')}}" alt="" style="width: 50%">
                    </div>
                </div>
                <div class="left-content3">
                    <!-- section tittle -->
                    <div class="section-tittle section-tittle2 mt-30 mb-20">
                        <div class="front-text">
                            <h2 class="">BangKode</h2>
                        </div>
                        <div class="features-caption">
                            <p>BangKode merupakan website belajar coding berbahasa indonesia secara lengkap dan materi yang disampaikan dijelaskan secara terstruktur step by step. Bangkode  didedikasikan untuk membantu pengguna agar menguasai keterampilan pemrograman dengan cara yang interaktif, menyenangkan, dan efektif.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Area End -->
        <!--? About Area-1 Start -->
        <section class="about-area1 fix pt-10">
            <div class="support-wrapper align-items-center">
                <div class="left-content1">
                    <!-- section tittle -->
                    <div class="section-tittle section-tittle2 mb-55">
                        <div class="front-text mt-100">
                            <p>BangKode adalah platform pembelajaran pemrograman yang menyediakan materi terstruktur dengan contoh kode praktis. Kami memahami kesulitan pemula dan menawarkan pengalaman pembelajaran yang menyenangkan. Dalam BangKode, pengguna dapat mempelajari pemrograman mulai dari tingkat pemula hingga lanjutan dengan bantuan video tutorial berkualitas tinggi. Kami berkomitmen untuk mengembangkan konten pembelajaran terbaik agar pengguna tetap berada di depan dalam dunia pemrograman yang terus berkembang.</p>
                        </div>
                    </div>
                </div>
                <div class="right-content1 mb-30">
                    <div class="right-img">
                        <div class="video-icon embed-responsive embed-responsive-16by9" >
                            {{-- <a class="popup-video btn-icon" href="https://www.youtube.com/watch?v=up68UAfH0d0"><i class="fas fa-play"></i></a> --}}
                            <iframe class="popup-video btn-icon fas fa-play" width="560" height="315" src="https://www.youtube-nocookie.com/embed/Y_9t3eQFmU4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Area End -->
@endsection
