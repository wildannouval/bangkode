@extends('dashboardAdmin')

@section('content')
<h2>Formulir Tambah Materi</h2>
<form action="{{ route('materi.store') }}" method="POST" enctype="multipart/form-data">
                        
    @csrf

    <div class="form-group">
        <label class="font-weight-bold">Judul Materi</label>
        <input type="text" class="form-control @error('judul_materi') is-invalid @enderror" name="judul_materi" value="{{ old('judul_materi') }}" placeholder="Masukkan Judul Materi">
    
        <!-- error message untuk title -->
        @error('judul_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label class="font-weight-bold">Url Materi</label>
        <input type="text" class="form-control @error('url_materi') is-invalid @enderror" name="url_materi" value="{{ old('url_materi') }}" placeholder="Masukkan Url Materi">
    
        <!-- error message untuk title -->
        @error('url_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label class="font-weight-bold">Deskripsi Materi</label>
        <textarea class="form-control @error('deskripsi_materi') is-invalid @enderror" name="deskripsi_materi" rows="5" placeholder="Masukkan Deskripsi Materi">{{ old('deskripsi_materi') }}</textarea>
    
        <!-- error message untuk content -->
        @error('deskripsi_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="id_topik">Topik</label>
        <select class="form-control" name="id_topik" id="id_topik">
            <option value="">--Pilih Topik--</option>
            @forelse ($topik as $item)
                <option value="{{$item->id_topik}}">{{$item->nama_topik}}</option>
            @empty
                <option value="">Belum ada data topik</option>
            @endforelse
        </select>
        </div>
        @error('id_topik')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
    <button type="submit" class="btn btn-md btn-primary">SIMPAN</button>
    <button type="reset" class="btn btn-md btn-warning">RESET</button>

</form>
@endsection
