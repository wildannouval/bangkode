@extends('layout.master')

@section('page')
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Materi</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Courses area start -->
    <div class="courses-area section-padding40 fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8">
                    <div class="section-tittle text-center mb-55">
                        <h2>{{ $topik->nama_topik }}</h2>
                    </div>
                </div>
            </div>
        </div>
        @forelse ($topik->materi()->get() as $item)
            <div class="row">
                <div class="container">
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header bg-info" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link btn-lg collapsed text-light" data-toggle="collapse"
                                        data-target="#{{ $item->id_materi }}" aria-expanded="true"
                                        aria-controls="collapseOne">
                                        {{ $item->judul_materi }}
                                    </button>
                                </h5>
                            </div>

                            <div id="{{ $item->id_materi }}" class="collapse" aria-labelledby="headingOne"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <div class="">
                                        <iframe width="560" height="315"
                                            src="https://www.youtube-nocookie.com/embed/{{ $item->url_materi }}"
                                            title="YouTube video player" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                            allowfullscreen></iframe>
                                    </div>
                                    <p>Source : https://www.youtube.com/{{ $item->url_materi }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- <div class="col-lg-4">
                    <div class="properties properties2 mb-30">
                        <div class="properties__card">
                            <div class="properties__img overlay1">
                                <a href="#"><img src="{{asset('image/'.$item->logo_topik)}}" alt=""></a>
                            </div>
                            <div class="properties__caption">
                                <p>{{ $item->nama_materi}}</p>
                            </div>
                            <div class="flex justify-around">
                                <a href="/materi/{{ $item->id_topik }}" class="border-btn border-btn2">Lihat Selanjutnya</a>
                            </div>
                            </div>
                        </div>
                    </div> --}}
                @empty
        @endforelse
    </div>
    </div>
    </div>
@endsection
