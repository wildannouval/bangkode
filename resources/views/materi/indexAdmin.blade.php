@extends('dashboardAdmin')

@section('title')
    List Materi
@endsection
@section('content')
    <a href="{{ route('materi.create') }}" class="btn btn-md btn-success mb-3">Tambah Materi</a>
    <table class="table table-bordered data">
        <thead>
            <tr>
                <th scope="col">ID Materi</th>
                <th scope="col">Judul Materi</th>
                <th scope="col">Url Materi</th>
                <th scope="col">Deskripsi Materi</th>
                <th scope="col">Topik</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($materis as $materi)
                <tr>
                    <td>{{ $materi->id_materi }}</td>
                    <td>{{ $materi->judul_materi }}</td>
                    <td>{{ $materi->url_materi }}</td>
                    <td>{{ $materi->deskripsi_materi }}</td>
                    <td>
                        @foreach ($materi->topik()->get() as $topik)
                            {{ $topik->nama_topik }}
                        @endforeach
                    </td>
                    <td class="text-center">
                        <form action="{{ route('materi.destroy', $materi->id_materi) }}" method="POST">
                            <a href="{{ route('materi.show', $materi->id_materi) }}" class="btn btn-sm btn-dark">SHOW</a>
                            <a href="{{ route('materi.edit', $materi->id_materi) }}" class="btn btn-sm btn-primary">EDIT</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-sm btn-danger show_confirm">HAPUS</button>
                        </form>
                    </td>
                </tr>
            @empty
                <div class="alert alert-danger">
                    Data Materi belum Tersedia.
                </div>
            @endforelse
        </tbody>
    </table>
    {{ $materis->links() }}
    
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            $('.show_confirm').click(function(event) {
                var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                        title: `Anda yakin menghapus data ini?`,
                        text: "Klik OK untuk menghapus",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        </script>
    @endpush

@endsection
