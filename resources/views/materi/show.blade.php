@extends('dashboardAdmin')

@section('content')
    <div class="embed-responsive embed-responsive-16by9" width="250px">
        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/{{ $materi->url_materi }}"
            title="YouTube video player" frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen></iframe>
    </div>
    <hr>
    <h4>Judul Materi : {{ $materi->judul_materi }}</h4>
    <p class="tmt-3">
        URL Materi : https://www.youtube-nocookie.com/embed/{{ $materi->url_materi }}
    </p>
    <p class="tmt-3">
        Deskripsi Materi : {{ $materi->deskripsi_materi }}
    </p>
@endsection
