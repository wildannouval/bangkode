@extends('dashboardAdmin')

@section('title')
Formulir Edit Materi
@endsection
@section('content')
<form action="{{ route('materi.update', $materi->id_materi) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label class="font-weight-bold">ID Materi</label>
        <input type="text" class="form-control @error('id_materi') is-invalid @enderror" name="id_materi" value="{{ old('id_materi', $materi->id_materi) }}">
    
        <!-- error message untuk title -->
        @error('id_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label class="font-weight-bold">Judul Materi</label>
        <input type="text" class="form-control @error('judul_materi') is-invalid @enderror" name="judul_materi" value="{{ old('judul_materi', $materi->judul_materi) }}" placeholder="Masukkan Judul Materi">
    
        <!-- error message untuk title -->
        @error('judul_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label class="font-weight-bold">Url Materi</label>
        <input type="text" class="form-control @error('url_materi') is-invalid @enderror" name="url_materi" value="{{ old('url_materi', $materi->url_materi) }}" placeholder="Masukkan Url Materi">
    
        <!-- error message untuk title -->
        @error('url_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>
    
    <div class="form-group">
        <label class="font-weight-bold">Deskripsi Materi</label>
        <textarea class="form-control @error('deskripsi_materi') is-invalid @enderror" name="deskripsi_materi" rows="5" placeholder="Masukkan Deskripsi Materi">{{ old('deskripsi_materi', $materi->deskripsi_materi) }}</textarea>
    
        <!-- error message untuk content -->
        @error('deskripsi_materi')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label class="font-weight-bold">Topik</label>
        <input type="text" class="form-control @error('id_topik') is-invalid @enderror" name="id_topik" value="{{ old('id_topik', $materi->id_topik) }}" placeholder="Masukkan Topik">
    
        <!-- error message untuk title -->
        @error('id_topik')
            <div class="alert alert-danger mt-2">
                {{ $message }}
            </div>
        @enderror
    </div>

    <button type="submit" class="btn btn-md btn-primary">UPDATE</button>
    <button type="reset" class="btn btn-md btn-warning">RESET</button>
</form> 
@endsection