@extends('dashboardAdmin')

@section('title')
    List Topik
@endsection

@section('content')


    <a href="/topik/create" type="button" class="btn btn-success mb-3">Tambah</a>
    <table class="table table-striped data">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nama Topik</th>
                <th scope="col">Logo</th>
                <th scope="col">Kategori</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($topik as $key => $item)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $item->nama_topik }}</td>
                    <td><img src="{{ asset('image/' . $item->logo_topik) }}" alt="gambar" height="50"></td>
                    <td>
                        @foreach ($item->kategori()->get() as $kat)
                            {{ $kat->nama_kategori }}
                        @endforeach
                    </td>
                    <td>
                        <form action="/topik/{{ $item->id_topik }}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/topik/{{ $item->id_topik }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Hapus" class="btn btn-danger btn-sm show_confirm">
                        </form>
                    </td>
                </tr>
            @empty
                <div class="alert alert-danger">
                    Data Topik belum Tersedia.
                </div>
            @endforelse
        </tbody>
    </table>

    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
            $('.show_confirm').click(function(event) {
                var form = $(this).closest("form");
                var name = $(this).data("name");
                event.preventDefault();
                swal({
                        title: `Anda yakin menghapus data ini?`,
                        text: "Klik OK untuk menghapus",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            form.submit();
                        }
                    });
            });
        </script>
    @endpush

@endsection
