@extends('dashboardAdmin')

@section('content')
<h2>Formulir Edit Topik</h2>
<form action="/topik/{{ $topik->id_topik }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="nama_topik">Nama Topik</label>
        <input type="text" class="form-control" id="nama_topik" name='nama_topik' value="{{ $topik->nama_topik }}">
    </div>
    @error('nama_topik')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="logo_topik">Logo</label>
        <input type="file" class="form-control" id="logo_topik" name='logo_topik' value="{{ $topik->logo_topik }}"><img src="{{ asset('image/'.$topik->logo_topik) }}" alt="" height="100px">
    </div>  
    @error('logo_topik')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="id_kategori">Kategori</label>
      <select class="form-control" name="id_kategori" id="id_kategori">
          <option value="">--Pilih Kategori--</option>
          @forelse ($kategori as $item)
                @if ($item->id_kategori === $topik->id_kategori)
                    <option value="{{$item->id_kategori}}" selected>{{$item->nama_kategori}}</option>    
                @else
                    <option value="{{$item->id_kategori}}">{{$item->nama_kategori}}</option>                     
                @endif
            @empty
                <option value="">Belum ada data kategori</option>
            @endforelse
      </select>
      </div>
      @error('id_kategori')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
@endsection