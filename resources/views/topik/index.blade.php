@extends('layout.master')

@section('page')
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Topik</h1>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>
    <!-- Courses area start -->
    <div class="courses-area section-padding40 fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-7 col-lg-8">
                    <div class="section-tittle text-center mb-55">
                        <h2>{{ $kategori->nama_kategori }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @forelse ($kategori->topik()->get() as $item )
                <div class="col-lg-4">
                    <div class="properties properties2 mb-30">
                        <div class="properties__card">
                            <div class="properties__img overlay1">
                                <a href="#"><img src="{{asset('image/'.$item->logo_topik)}}" alt=""></a>
                            </div>
                            <div class="properties__caption">
                                <p>{{ $item->nama_topik}}</p>
                            </div>
                            <div class="flex justify-around">
                                <a href="/course/{{ $item->id_kategori }}/{{ $item->id_topik }}" class="border-btn border-btn2">Lihat Selanjutnya</a>
                            </div>
                            </div>
                        </div>
                    </div>
                @empty
                    
                @endforelse
            </div>
        </div>
    </div>
@endsection
