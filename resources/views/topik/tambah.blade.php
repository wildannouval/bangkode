@extends('dashboardAdmin')

@section('content') 

<h2>Formulir Tambah Topik</h2>
<form action="/topik" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="nama_topik">Nama Topik</label>
        <input type="text" class="form-control" id="nama_topik" name='nama_topik'>
    </div>
    @error('nama_topik')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="logo_topik">Logo</label>
        <input type="file" class="form-control" id="logo_topik" name='logo_topik'>
    </div>  
    @error('logo_topik')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="id_kategori">Kategori</label>
      <select class="form-control" name="id_kategori" id="id_kategori">
          <option value="">--Pilih Kategori--</option>
          @forelse ($kategori as $item)
              <option value="{{$item->id_kategori}}">{{$item->nama_kategori}}</option>
          @empty
              <option value="">Belum ada data kategori</option>
          @endforelse
      </select>
      </div>
      @error('id_kategori')
          <div class="alert alert-danger">{{$message}}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
