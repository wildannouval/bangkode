@extends('dashboardAdmin')

@section('content') 
    <h2>Formulir Edit Kategori</h2>
    <form action="/kategori/{{$kategori->id_kategori}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="nama_kategori">Nama Kategori</label>
            <input type="text" class="form-control" id="nama_kategori" name='nama_kategori' value="{{$kategori->nama_kategori}}">
        </div>
        @error('nama_kategori')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label for="foto">Foto</label>
            <input type="file" class="form-control" id="foto" name='foto' value="{{$kategori->foto}}"><img src="{{ asset('/image/' . $kategori->foto) }}" height="100px">
        </div>  
        @error('foto')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
{{-- @section('link')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.3.1/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: ".formcategory",
        forced_root_block : "false",
        height: 350,
    })
</script>
@endsection  --}}