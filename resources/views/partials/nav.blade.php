{{-- <header> --}}
    <!-- Header Start -->
    <div class="header-area header-transparent">
        <div class="main-header ">
            <div class="header-bottom header-sticky">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo">
                                <a href="index.html"><img src="{{asset('bangkode_logo_white.png')}}" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="menu-wrapper d-flex align-items-center justify-content-end">
                                <!-- Main-menu -->
                                <div class="main-menu d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">                                                                                          
                                            <li class="active" ><a href="/">Home</a></li>
                                            <li><a href="/course">Courses</a></li>
                                            <li><a href="/about">About</a></li>
                                            <li><a href="/profile">Profile</a></li>
                                            @if(Auth::User()->role == "admin")
                                            <li><a href="/dashboard">Dashboard</a></li>
                                            @endif
                                            <!-- Button -->
                                            @if (Route::has('login'))
                                                @auth
                                                    <li>
                                                        <form method="POST" action="{{ route('logout') }}" class="button-header">
                                                            @csrf
                                        
                                                            <x-responsive-nav-link :href="route('logout')" class="btn btn3"
                                                                    onclick="event.preventDefault();
                                                                                this.closest('form').submit();">
                                                                {{ __('Log Out') }}
                                                            </x-responsive-nav-link>
                                                        </form>
                                                    </li>
                                                @else
                                                @if (Route::has('register'))
                                                    <li class="button-header margin-left "><a href="{{ route('register') }}" class="btn">Join</a></li>
                                                @endif
                                                <li class="button-header">

                                                    <a href="{{ route('login') }}" class="btn btn3">Log in</a>
                                                </li>
                                                @endauth
                                            </div>
                                        @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div> 
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
{{-- </header> --}}