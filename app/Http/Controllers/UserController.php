<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\View\View;

class UserController extends Controller
{
    public function tampiluser(): View
    {
        //get posts
        $user = User::latest()->paginate(5);

        //render view with posts
        return view('user.indexAdmin', compact('user'));
    }
}
